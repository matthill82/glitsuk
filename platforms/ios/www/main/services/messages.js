/**
 * Created by mhill168 on 14/07/15.
 */

(function () {

    angular.module('glits').factory('messages', function(){

        var messages = {};

        messages.list = [];

        messages.add = function(message){
            messages.list.push({
                id: messages.list.length + 1,
                duration: "(" + message.duration + ")",
                text: message.text,
                direction: message.method
            });
        };

        return messages;
    });

})();


