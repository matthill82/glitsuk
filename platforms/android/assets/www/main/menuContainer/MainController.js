/**
 * Created by matthewhill on 05/07/15.
 */

(function () {

  angular.module('glits').controller('MainController', ['$scope', '$rootScope', 'localStorageService', MainController]);

  function MainController ($scope, $rootScope, localStorageService) {

    $scope.getStorage = function () {
      var inStore = localStorageService.get('shortlists');
      $rootScope.shortlists = inStore || [];
    };

    $scope.$watch('shortlists', function () {
      localStorageService.set('shortlists', $rootScope.shortlists);
    }, true);

    $scope.getStorage();

  }
})();
