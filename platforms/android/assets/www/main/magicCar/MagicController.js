/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

    'use strict';

    angular.module('glits').controller('MagicController', ['$scope', '$ionicLoading', MagicController]);

    function MagicController ($scope, $ionicLoading) {

        $scope.navTitle = 'Magic Car';

        var currentPlatform = ionic.Platform.platform();
        $scope.currentPlatform = currentPlatform;

        (currentPlatform == "ios" ?
            $scope.magicLink = "itms-apps://itunes.apple.com/gb/app/aurasma/id432526396?mt=8" :
                (currentPlatform == "android" ?
                    $scope.magicLink = "market://play.google.com/store/apps/details?id=com.aurasma.aurasma" : "")
        );
    }

})();
