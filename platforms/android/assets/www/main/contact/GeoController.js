/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

  'use strict';

  angular.module('glits').controller('GeoController', ['$scope', '$ionicLoading', '$timeout', '$cordovaGeolocation', GeoController]);

  function GeoController($scope, $ionicLoading, $timeout, $cordovaGeolocation) {

    $scope.navTitle = 'Contact';

    $scope.rightButtons = [{
      type: 'button-clear',
      content: 'Menu',
      tap: function (e) {
        $scope.openModal();
      }
    }];


    $scope.data = {
      isLoading: true
    };

    $timeout(function () {
      $scope.data = {
        isLoading: false
      };
    }, 500);

    var posOptions = {timeout: 10000, enableHighAccuracy: false};

    $scope.map = {
      center: {
        latitude: 51.5830149,
        longitude: 0.4037189
      },
      zoom: 15,
      id: 0,
      draggable: false
    };

    $scope.options = {
      scrollwheel: false
    };

    $scope.marker = {
      coords: {
        latitude: $scope.map.center.latitude,
        longitude: $scope.map.center.longitude
      }
    };

    $scope.centerOnGlits = function () {

      $scope.map = {
        center: {
          latitude: 51.5830149,
          longitude: 0.4037189
        },
        zoom: 15,
        id: 0,
        draggable: false
      };

      $scope.options = {
        scrollwheel: false
      };

      $scope.marker = {
        coords: {
          latitude: 51.5830149,
          longitude: 0.4037189
        }
      };

    };

    $scope.centerOnMe = function () {
      if (!$scope.map) return;

      $scope.data = {
        isLoading: true
      };

      $cordovaGeolocation.getCurrentPosition(posOptions)
        .then(function (pos) {

          $scope.map = {
            center: {
              latitude: pos.coords.latitude,
              longitude: pos.coords.longitude
            },
            zoom: 16,
            id: 0
          };

          $scope.marker.coords = {
            latitude: pos.coords.latitude,
            longitude: pos.coords.longitude
          };

          $timeout(function () {
            $scope.data.isLoading = false;
          }, 500);


        },
        $scope.showError);
    };

    $scope.showError = function (error) {
      switch (error.code) {
        case error.PERMISSION_DENIED:
          $scope.error = "User denied the request for Geolocation.";
          break;
        case error.POSITION_UNAVAILABLE:
          $scope.error = "Location information is unavailable.";
          break;
        case error.TIMEOUT:
          $scope.error = "The request to get user location timed out.";
          break;
        case error.UNKNOWN_ERROR:
          $scope.error = "An unknown error occurred.";
          break;
      }
      $scope.$apply();
    };

  }

})();

