/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

    'use strict';

    angular.module('glits').controller('SeminarsController', ['$scope', 'glitzService', '$ionicScrollDelegate', SeminarsController]);

    function SeminarsController ($scope, glitzService, $ionicScrollDelegate) {

        $scope.navTitle = 'Seminars';

        $scope.rightButtons = [{
            type: 'button-clear',
            content: 'Menu',
            tap: function (e) {
                $scope.openModal();
            }
        }];

        var vm = this;

        vm.data = {
            isLoading : true
        };

        vm.scrollTop = function () {
          $ionicScrollDelegate.scrollTop(true);
        };

        vm.getScrollPosition = function() {
          //monitor the scroll
          vm.moveData = $ionicScrollDelegate.getScrollPosition().top;
          if(vm.moveData>=250){
            $('.scrollTop').fadeIn();
          }else if(vm.moveData<250){
            $('.scrollTop').fadeOut();
          }
        };

        glitzService.getFeedData().then(function (data) {
            if(data) {
                vm.seminars = data.seminars;
                vm.data.isLoading = false;
            } else {
                return false;
            }
        })
    }

})();
