/**
 * Created by mhill168 on 04/09/2015.
 */

(function () {

  angular.module('glits').factory('AuthService', function($rootScope) {

    var loggedIn=false;

    return {

      checkLogin : function() {
        $rootScope.$broadcast('loggedIn', { 'loggedIn' : loggedIn });
        return loggedIn;

      },

      login : function() {
        loggedIn = true;
        $rootScope.$broadcast('loggedIn', { 'loggedIn' : loggedIn });
      }

    }

  })

})();
