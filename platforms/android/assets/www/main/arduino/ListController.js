/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

    angular.module('glits').controller('ListController', function (messages){

        var self = this;

        self.messages = messages.list;

        self.shouldShowDelete = false;
        self.shouldShowReorder = true;

        self.moveItem = function(message, fromIndex, toIndex) {
            //Move the item in the array
            self.messages.splice(fromIndex, 1);
            self.messages.splice(toIndex, 0, message);
        };

        self.deleteMessage = function (message) {
           self.messages.splice(self.messages.indexOf(message), 1);
        }
    });

})();

