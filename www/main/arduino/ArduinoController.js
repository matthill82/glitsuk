/**
 * Created by matthewhill on 04/07/15.
 */

(function () {

    angular.module('glits').controller('ArduinoController', ['$scope', '$rootScope', 'localStorageService', '$ionicModal', 'messages', '$ionicLoading', '$ionicActionSheet', '$timeout', '$http', ArduinoController]);

    function ArduinoController($scope, $rootScope, localStorageService, $ionicModal, messages, $ionicLoading, $ionicActionSheet, $timeout, $http) {


        $scope.navTitle = 'Arduino';

        $scope.rightButtons = [{
            type: 'button-clear',
            content: 'Menu',
            tap: function (e) {
                $scope.openModal();
            }
        }];

        // Load the modal from the given template URL
        $ionicModal.fromTemplateUrl('main/arduino/addDirections.html', function($ionicModal) {
                $scope.modal = $ionicModal;
            }, {
                // Use our scope for the scope of the modal to keep it simple
                scope: $scope,
                // The animation we want to use for the modal entrance
                animation: 'slide-in-up'
            });

            $scope.openModal = function() {
                $scope.modal.show();
            };
            $scope.closeModal = function() {
                $scope.modal.hide();
            };
            $scope.toggleModal = function () {
                if($scope.modal.isShown()) {
                    $scope.closeModal()
                } else {
                    $scope.openModal()
                }
            };
            //Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function() {
                $scope.modal.remove();
            });

        $scope.showDetails = function() {
            $ionicActionSheet.show({
                titleText: 'Send your Code',
                buttons: [
                    { text: '<i class="icon ion-email"></i> Email Code' }
                ],
                cancelText: 'Cancel',
                cancel: function() {
                    console.log('CANCELLED');
                },
                buttonClicked: function() {
                  $scope.processForm();
                  return true;
                }
            });
        };

        $scope.formData = messages.list;
        var credentials = localStorageService.get('credentials');

        console.log(credentials.name);
        console.log(credentials.cdsId);

      $scope.processForm = function () {
          console.log($scope.formData);
          console.log(credentials);
        if($scope.formData.length) {
          $http({
            method: 'POST',
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: 'http://www.concard.co.uk/dbwrite.php',
            data: $.param({name: credentials.name, cdsId: credentials.cdsId, code : $scope.formData })
          })
            .success(function (data) {
              if(data) {
                $timeout(function () {
                  $scope.showSuccess();
                }, 250);
              }
            }, function (error) {
                  $scope.showError();
              return false;
            })
        } else {
            $scope.showError();
        }
      };

      $scope.showSuccess = function () {
        $ionicLoading.show({
          duration: 2000,
          maxWidth: 350,
          noBackdrop: true,
          template: '<ion-spinner icon="ripple" class="spinner-balanced block"></ion-spinner> <br/> <p class="text-center">Data Successfully sent</p>'
        });
      };

      $scope.showError = function () {
        $ionicLoading.show({
          duration: 2000,
          maxWidth: 350,
          noBackdrop: true,
          template: '<ion-spinner icon="ripple" class="spinner-assertive block"></ion-spinner> <br/> <p class="text-center">You\'ve nothing to send</p>'
        });
      }

    }

})();

