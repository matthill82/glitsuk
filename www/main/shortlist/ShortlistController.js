/**
 * Created by mhill168 on 21/07/2015.
 */

(function () {

  'use strict';

  angular.module('glits').controller('ShortlistController', ['$scope', '$rootScope', '$state', 'localStorageService', '$timeout', ShortlistController]);

  function ShortlistController($scope, $rootScope, $state, localStorageService, $timeout) {

    $scope.navTitle = "Shortlists";

    $scope.rightButtons = [{
      type: 'button-clear',
      content: 'Menu',
      tap: function (e) {
        $scope.openModal();
      }
    }];

    var vm = this;
    vm.data = {
      isLoading: true
    };

    $scope.callStorage = function () {
      var inStore = localStorageService.get('shortlists');
      $rootScope.shortlists = inStore || [];

      $scope.$watch('shortlists', function () {
        localStorageService.set('shortlists', $rootScope.shortlists);
      }, true);

      $timeout(function () {
        vm.data.isLoading = false;
      }, 500);
    };

    $scope.callStorage();

    // List Toggles
    $scope.editBtnText = 'Remove';
    $scope.toggleDelete = function() {
      $scope.isDeletingItems = !$scope.isDeletingItems;
      $scope.isReorderingItems = false;
      $scope.editBtnText = ($scope.isDeletingItems ? 'Done' : 'Delete');
    };

    $scope.reorderBtnText = 'Reorder';
    $scope.toggleReorder = function() {
      $scope.isReorderingItems = !$scope.isReorderingItems;
      $scope.isDeletingItems = false;
      $scope.reorderBtnText = ($scope.isReorderingItems ? 'Done' : 'Reorder');
    };

    $scope.moveItem = function(item, fromIndex, toIndex) {
      //Move the item in the array
      $rootScope.shortlists.splice(fromIndex, 1);
      $rootScope.shortlists.splice(toIndex, 0, item);
      localStorageService.set('shortlists', $rootScope.shortlists);
    };

    $scope.removeShortlist = function(item) {
      console.log($rootScope.shortlists.length);
      if($rootScope.shortlists.length <= 1) {
        $scope.editBtnText = 'Delete';
      }
      console.log('onDelete from the "list" on-delete attribute', item);
      $rootScope.shortlists.splice($rootScope.shortlists.indexOf(item), 1);
    };
  }

})();
