
(function () {

  'use strict';

  angular.module('glits').controller('SigninController', function($scope, $rootScope, $state, $timeout, localStorageService) {

      $rootScope.data = {};

      $scope.login = function() {

        if($rootScope.data) {
          localStorageService.set('credentials', $rootScope.data);
          console.log("LOGIN name: " + $rootScope.data.name + " - cdsId: " + $rootScope.data.cdsId);
          $state.go('main.home');
        } else {
          return false;
        }
      }

  });


})();
